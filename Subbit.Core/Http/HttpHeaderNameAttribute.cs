﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Subbit.Core.Http
{
    public class HttpHeaderNameAttribute : Attribute
    {
        public HttpHeaderNameAttribute(string headerName)
        {
            HeaderName = headerName;
        }

        public string HeaderName { get; set; }
    }
}
