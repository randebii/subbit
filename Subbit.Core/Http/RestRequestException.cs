﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace Subbit.Core.Http
{
    public class RestRequestException : HttpRequestException
    {
        public RestRequestException() : base() { }
        public RestRequestException(HttpResponseMessage response, string message) : base(message)
        {
            Response = response;
        }
        public RestRequestException(HttpResponseMessage response, string message, Exception inner) : base(message, inner)
        {
            Response = response;
        }

        public HttpResponseMessage Response { get; set; }
    }
}
