﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;

namespace Subbit.Core.Http
{
    public class RestParameters
    {
        public RestParameters(string url, string accept = null, string authorization = null, string contentType = null, NameValueCollection queryStrings = null, string userAgent = null)
        {
            Uri = new Uri(url);
            Accept = accept;
            Authorization = authorization;
            ContentType = contentType;
            QueryStrings = queryStrings == null ? new NameValueCollection() : new NameValueCollection(queryStrings);
            UserAgent = userAgent;
        }

        public Uri Uri { get; set; }

        public string Accept { get; set; }

        public string Authorization { get; set; }        

        [HttpHeaderName("User-Agent")]
        public string UserAgent { get; set; }

        [HttpHeaderName("Content-Type")]
        public string ContentType { get; set; }

        public NameValueCollection QueryStrings { get; set; }
    }
}
