﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Subbit.Core.Http
{
    public static class RestClient
    {
        public static async Task<string> GetStringAsync(RestParameters parameters)
        {
            var response = await GetAsync(parameters);
            string stringContent = await response.Content.ReadAsStringAsync();
            return stringContent;
        }

        public static async Task<byte[]> GetBytesAsync(RestParameters parameters)
        {
            var response = await GetAsync(parameters);
            byte[] byteContent = await response.Content.ReadAsByteArrayAsync();
            return byteContent;
        }

        private static async Task<HttpResponseMessage> GetAsync(RestParameters parameters)
        {
            var client = CreateHttpClient(parameters);
            string url = FormatRequestUrlWithQueryStrings(parameters);
            HttpResponseMessage response = await client.GetAsync(url);
            if (!response.IsSuccessStatusCode)
            {
                throw new RestRequestException(response, $"{response.ReasonPhrase} ({response.StatusCode})");
            }
            return response;
        }

        private static string FormatRequestUrlWithQueryStrings(RestParameters parameters)
        {
            var builder = new UriBuilder(parameters.Uri);
            builder.Port = -1;

            if (parameters.QueryStrings.Count > 0)
            {
                var query = HttpUtility.ParseQueryString(builder.Query);
                for (int i = 0; i < parameters.QueryStrings.Count; i++)
                {
                    query[parameters.QueryStrings.GetKey(i)] = parameters.QueryStrings.Get(i);
                }
                builder.Query = query.ToString();
            }
            
            return builder.ToString();
        }

        private static HttpClient CreateHttpClient(RestParameters parameters)
        {
            var client = new HttpClient();

            //client.BaseAddress = parameters.BaseUri;

            var properties = parameters.GetType().GetProperties();
            foreach(var prop in properties)
            {
                if (prop.Name == nameof(parameters.Uri) ||
                    prop.Name == nameof(parameters.QueryStrings))
                    continue;

                object value = prop.GetValue(parameters);
                if (value != null)
                {
                    string name = prop.Name;
                    foreach (var item in prop.GetCustomAttributes(true))
                    {
                        if (item is HttpHeaderNameAttribute)
                        {
                            name = (item as HttpHeaderNameAttribute).HeaderName;
                            break;
                        }
                    }

                    if (prop.PropertyType == typeof(string) && !string.IsNullOrEmpty(value.ToString()))
                    {
                        client.DefaultRequestHeaders.Add(name, value.ToString());
                    }
                }
            }
            
            return client;
        }
    }
}
