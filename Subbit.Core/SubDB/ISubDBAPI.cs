﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Subbit.Core.SubDB
{
    public interface ISubDBAPI
    {
        string ProtocolName { get; }
        string ProtocolVersion { get; }
        string ClientName { get; }
        string ClientVersion { get; }
        Uri ClientURI { get; }

        Task<IEnumerable<string>> GetLanguagesAsync();
        Task<IEnumerable<string>> SearchSubtitleAsync(string hash, int? versions = null);
        Task<byte[]> DownloadSubtitleAsync(string hash, string language);
    }
}
