﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Subbit.Core.SubDB
{
    public abstract class SubDBAPI : ISubDBAPI
    {
        public SubDBAPI(string protocolName, string protocolVersion, string clientName, string clientVersion, string clientUrl)
        {
            ProtocolName = protocolName;
            ProtocolVersion = protocolVersion;
            ClientName = clientName;
            ClientVersion = clientVersion;
            ClientURI = new Uri(clientUrl);
        }

        public string ProtocolName { get; protected set; }
        public string ProtocolVersion { get; protected set; }
        public string ClientName { get; protected set; }
        public string ClientVersion { get; protected set; }
        public Uri ClientURI { get; protected set; }

        public abstract Task<byte[]> DownloadSubtitleAsync(string hash, string language);
        public abstract Task<IEnumerable<string>> GetLanguagesAsync();
        public abstract Task<IEnumerable<string>> SearchSubtitleAsync(string hash, int? versions = null);
    }
}
