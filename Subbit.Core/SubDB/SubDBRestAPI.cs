﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Subbit.Core.Http;

namespace Subbit.Core.SubDB
{
    public sealed class SubDBRestAPI : SubDBAPI
    {
        private enum QNames
        {
            action,
            hash,
            language,
            versions
        }

        private enum QActions
        {
            search,
            languages,
            download
        }

        private const string API_URL = "http://api.thesubdb.com/"; //"http://sandbox.thesubdb.com/";

        private Action<string, string> AddQS;
        private Action ClearQS;

        public SubDBRestAPI(string clientName, string clientVersion, string clientUrl, string protocolName = "SubDB", string protocolVersion = "1.0")
            : base(protocolName, protocolVersion, clientName, clientVersion, clientUrl)
        {
            RestParameters = new RestParameters(API_URL, userAgent: UserAgent);
            AddQS = RestParameters.QueryStrings.Add;
            ClearQS = RestParameters.QueryStrings.Clear;
        }

        public RestParameters RestParameters { get; private set; }
        public string UserAgent
        {
            get
            {
                return $"{ProtocolName}/{ProtocolVersion} ({ClientName}/{ClientVersion}; {ClientURI.AbsoluteUri})";
            }
        }

        public override async Task<byte[]> DownloadSubtitleAsync(string hash, string languages)
        {
            ClearQS();
            AddQS(QNames.action.ToString(), QActions.download.ToString());
            AddQS(QNames.hash.ToString(), hash);
            AddQS(QNames.language.ToString(), languages);

            byte[] subtitleBytes = null;
            try
            {
               subtitleBytes = await RestClient.GetBytesAsync(RestParameters);
            }
            catch (RestRequestException e)
            {
                if (e.Response.StatusCode != HttpStatusCode.NotFound)
                    throw;
            }

            return subtitleBytes;
        }

        public override async Task<IEnumerable<string>> GetLanguagesAsync()
        {
            ClearQS();
            AddQS(QNames.action.ToString(), QActions.languages.ToString());

            string languages = await RestClient.GetStringAsync(RestParameters);
            var languageList = SplitResultToList(languages);            

            return languageList;
        }

        public override async Task<IEnumerable<string>> SearchSubtitleAsync(string hash, int? versions = null)
        {
            ClearQS();
            AddQS(QNames.action.ToString(), QActions.search.ToString());
            AddQS(QNames.hash.ToString(), hash);
            if (versions.HasValue)
            {
                AddQS(QNames.versions.ToString(), versions.Value.ToString());
            }

            List<string> searchResultList = null;
            try
            {
                string searchResults = await RestClient.GetStringAsync(RestParameters);
                searchResultList = SplitResultToList(searchResults);
            }
            catch (RestRequestException e)
            {
                if (e.Response.StatusCode != HttpStatusCode.NotFound)
                    throw;
            }

            return searchResultList;
        }
        
        private static List<string> SplitResultToList(string languages)
        {
            var languageList = new List<string>();
            if (!string.IsNullOrEmpty(languages))
            {
                var split = languages.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var item in split)
                {
                    languageList.Add(item);
                }
            }

            return languageList;
        }

    }
}
