﻿using System;
using System.IO;
using System.Security.Cryptography;

namespace Subbit.Core
{
    public class Media
    {
        private static MD5 md5Hash = MD5.Create();
        private readonly int HASH_SIZE = 64 * 1024;

        public Media(string path)
        {
            FilePath = path;
        }
        
        public string FilePath { get; set; }
        
        public string ComputeHash()
        {
            if (!File.Exists(FilePath))
                throw new FileNotFoundException($"File does not exist: {FilePath}");
            
            var data = new byte[HASH_SIZE * 2];
            
            using (var reader = new BinaryReader(new FileStream(FilePath, FileMode.Open, FileAccess.Read)))
            {
                reader.Read(data, 0, HASH_SIZE);

                reader.BaseStream.Seek(-HASH_SIZE, SeekOrigin.End);

                reader.Read(data, HASH_SIZE, HASH_SIZE);
            }

            var byteHash = md5Hash.ComputeHash(data);
            var hexHash = BitConverter.ToString(byteHash).Replace("-", string.Empty).ToLower();

            return hexHash;
        }
    }
}
