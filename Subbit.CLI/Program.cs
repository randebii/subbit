﻿using Subbit.Core;
using Subbit.Core.SubDB;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security;

namespace Subbit.CLI
{
    class Program
    {
        const string CLIENT_NAME = "Subbit";
        const string CLIENT_VERSION = "0.1";
        const string CLIENT_URL = "https://bitbucket.org/randebii/subbit";
        readonly static string[] SUPPORTED_EXTS = new string[]
        {
            ".mp4", ".m4p", ".avi", ".mkv", ".wmv", ".mpg", ".mpeg", ".mov", ".webm", ".asf"
        };

        static int Main(string[] args)
        {
            int exitCode = 0;

            try
            {
                if (args.Length == 0)
                    Console.WriteLine("Arguments expected");

                if (args[0].StartsWith("-"))
                {
                    Manage(args[0]);
                }
                else
                {
                    DownloadSubtitle(args);
                }
            }
            catch (NotSupportedException e)
            {
                Console.WriteLine(e.Message);
                DisplaySupportedExtentions();
                exitCode = -4;
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
                DisplayUsage();
                exitCode = -3;
            }
            catch (SecurityException e)
            {
                if (e.Message.Contains("Requested registry access is not allowed"))
                    Console.WriteLine($"{e.Message}. Try running as Administrator.");
                else
                    Console.WriteLine($"ERROR: {e.Message}");
                exitCode = -1;
            }
            catch (Exception e)
            {
                Console.WriteLine($"ERROR: {e.Message}");
                exitCode = -1;
            }

            return exitCode;
        }

        static void Manage(string arg)
        {
            switch(arg.ToLower())
            {
                case "-?":
                    DisplayUsage();
                    break;

                case "-install":
                    Install();
                    break;

                case "-uninstall":
                    Uninstall();
                    break;

                case "-supports":
                    DisplaySupportedExtentions();
                    break;

                case "-languages":
                    DisplayLanguages();
                    break;

                default:
                    throw new ArgumentException("Unknown command");
            }
        }

        static void Install()
        {
            string applicationPath = Assembly.GetExecutingAssembly().Location;
            var applicationFile = new FileInfo(applicationPath);

            RegistryManager.InstallAppPath(applicationFile.Name, applicationPath);

            string command = $"\"{applicationFile.Name}\" \"%1\"";

            foreach (var item in SUPPORTED_EXTS)
            {
                RegistryManager.InstallCommand(item, command);
            }
        }

        static void Uninstall()
        {
            string applicationPath = Assembly.GetExecutingAssembly().Location;
            var applicationFile = new FileInfo(applicationPath);

            RegistryManager.UninstallAppPath(applicationFile.Name);

            foreach(var item in SUPPORTED_EXTS)
            {
                RegistryManager.UninstallCommand(item);
            }
        }

        static void DisplayUsage()
        {
            Console.WriteLine("To download subtitles:\n");
            Console.WriteLine("SUBBIT-CLI full_path_to_file [language]\n");
            Console.WriteLine("For other commands:\n");
            Console.WriteLine("SUBBIT-CLI [command]");
            Console.WriteLine("  -? \t Display usage");
            Console.WriteLine("  -install \t Integrates the app to your registry and right-click context menu");
            Console.WriteLine("  -uninstall \t Removes the app from your registry and right-click context menu");
            Console.WriteLine("  -supports \t Lists the supported file extensions");
            Console.WriteLine("  -languages \t Lists the supported languages when downloading a subtitle\n");
        }

        static void DisplaySupportedExtentions()
        {
            Console.WriteLine("Supported file extensions:");
            foreach(var item in SUPPORTED_EXTS)
            {
                Console.WriteLine(item);
            }
        }

        static void DisplayLanguages()
        {
            ISubDBAPI subdb = new SubDBRestAPI(CLIENT_NAME, CLIENT_VERSION, CLIENT_URL);
            IEnumerable<string> languages = subdb.GetLanguagesAsync().Result;

            if (languages != null)
            {
                Console.WriteLine("Languages:");
                foreach (var item in languages)
                {
                    Console.WriteLine(item);
                }
            }
        }

        static void DownloadSubtitle(string[] args)
        {
            string filePath = null;
            string language = null;

            if (!File.Exists(filePath = args[0]))
                throw new FileNotFoundException($"File does not exist - {filePath}");

            var fileInfo = new FileInfo(filePath);
            if (!SUPPORTED_EXTS.Any(a => a == fileInfo.Extension))
                throw new NotSupportedException("File extension not supported.");

            if (args.Length == 2)
            {
                language = args[1];
            }
            else
            {
                language = "en";
            }

            var media = new Media(filePath);
            string hash = media.ComputeHash();

            ISubDBAPI subdb = new SubDBRestAPI(CLIENT_NAME, CLIENT_VERSION, CLIENT_URL);

            byte[] subtitle = subdb.DownloadSubtitleAsync(hash, language).Result;

            if (subtitle != null)
            {
                WriteSubtitle(media.FilePath, subtitle);
                Console.WriteLine("Subtitle downloaded successfully");
            }
            else
            {
                Console.WriteLine("No subtitle found. Press any key to continue.");
                Console.ReadKey();
            }
        }

        static void WriteSubtitle(string mediaPath, byte[] subtitle)
        {
            var file = new FileInfo(mediaPath);
            string subtitleFullPath = Path.Combine(file.DirectoryName, file.Name.Replace(file.Extension, ".srt"));

            using (FileStream stream = new FileStream(subtitleFullPath, FileMode.Create, FileAccess.Write))
            {
                stream.Write(subtitle, 0, subtitle.Length);
            }
        }
    }
}
