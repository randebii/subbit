﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Text;

namespace Subbit.CLI
{
    public static class RegistryManager
    {
        static string APP_PATH = @"SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths";
        static string SYS_FILE_ASSOC = "SystemFileAssociations";
        static string SHELL_KEY = "shell";
        static string DL_KEY = "Download subtitle";
        static string CMD_KEY = "command";

        public static void InstallAppPath(string appName, string appPath)
        {
            using (RegistryKey key = Registry.LocalMachine.OpenSubKey(APP_PATH, true))
            {
                bool appExists = DoesKeyExist(appName, key);

                if (!appExists)
                {
                    using (RegistryKey appKey = key.CreateSubKey(appName))
                    {
                        key.Flush();
                        appKey.SetValue(string.Empty, appPath);
                    }
                }
            }
        }

        private static bool DoesKeyExist(string keyName, RegistryKey key)
        {
            bool keyExists = false;

            foreach (var subKey in key.GetSubKeyNames())
            {
                if (subKey.ToLower() == keyName.ToLower())
                {
                    keyExists = true;
                    break;
                }
            }

            return keyExists;
        }

        public static void UninstallAppPath(string appName)
        {
            using (RegistryKey key = Registry.LocalMachine.OpenSubKey(APP_PATH, true))
            {
                bool appExists = DoesKeyExist(appName, key);

                if (appExists)
                {
                    key.DeleteSubKey(appName);
                }
            }
        }

        public static void InstallCommand(string keyName, string command)
        {
            using (RegistryKey classRootKey = Registry.ClassesRoot.OpenSubKey(SYS_FILE_ASSOC))
            {
                RegistryKey key = null;
                RegistryKey shellKey = null;
                RegistryKey downloadKey = null;
                RegistryKey commandKey = null;
                try
                { 
                    key = classRootKey.OpenSubKey(keyName, true);
                    if (key != null)
                    {
                        bool hasShell = DoesKeyExist(SHELL_KEY, key);
                        if (!hasShell)
                        {
                            key.CreateSubKey(SHELL_KEY);
                            key.Flush();
                        }

                        shellKey = key.OpenSubKey(SHELL_KEY, true);
                        bool hasDlKey = DoesKeyExist(DL_KEY, shellKey);
                        if (!hasDlKey)
                        {
                            shellKey.CreateSubKey(DL_KEY);
                            shellKey.Flush();
                        }

                        downloadKey = shellKey.OpenSubKey(DL_KEY, true);
                        bool hasCmdKey = DoesKeyExist(CMD_KEY, downloadKey);
                        if (!hasCmdKey)
                        {
                            downloadKey.CreateSubKey(CMD_KEY);
                            downloadKey.Flush();
                        }

                        commandKey = downloadKey.OpenSubKey(CMD_KEY, true);
                        commandKey.SetValue(string.Empty, command);
                    }
                }
                finally
                {
                    key?.Close();
                    shellKey?.Close();
                    downloadKey?.Close();
                    commandKey?.Close();
                }
            }
        }

        public static void UninstallCommand(string keyName)
        {
            using (RegistryKey classRootKey = Registry.ClassesRoot.OpenSubKey(SYS_FILE_ASSOC))
            {
                RegistryKey key = null;
                RegistryKey shellKey = null;
                RegistryKey downloadKey = null;
                RegistryKey commandKey = null;
                try
                {
                    key = classRootKey.OpenSubKey(keyName, true);
                    if (key != null)
                    {
                        bool hasShell = DoesKeyExist(SHELL_KEY, key);
                        if (hasShell)
                        {
                            shellKey = key.OpenSubKey(SHELL_KEY, true);

                            bool hasDlKey = DoesKeyExist(DL_KEY, shellKey);
                            if (hasDlKey)
                            {
                                shellKey.DeleteSubKeyTree(DL_KEY);
                            }
                        }
                    }
                }
                finally
                {
                    key?.Close();
                    shellKey?.Close();
                    downloadKey?.Close();
                    commandKey?.Close();
                }
            }
        }
    }
}
// "D:\Projects\Bitbucket\subbit\Subbit.CLI\bin\Release\netcoreapp2.0\win-x86\subbit-cli.exe" "%1"