﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Subbit.Core.SubDB;
using System.Linq;
using Subbit.Core;

namespace Subbit.Tests
{
    [TestClass]
    public class SubDBRestApiTests
    {
        private ISubDBAPI subdb;

        [TestInitialize]
        public void Init()
        {
            subdb = new SubDBRestAPI("Subbit", "0.1", "https://bitbucket.org/randebii/subbit");
        }

        [TestMethod]
        public void Languages()
        {
            var languanges = subdb.GetLanguagesAsync().Result;

            Assert.IsNotNull(languanges);
            Assert.IsTrue(languanges.Count() > 0);
        }

        [TestMethod]
        public void Search()
        {
            var dexter = new Media(@"SampleMedia\dexter.mp4");

            var results = subdb.SearchSubtitleAsync(dexter.ComputeHash()).Result;

            Assert.IsNotNull(results);
            Assert.IsTrue(results.Count() > 0);
        }

        [TestMethod]
        public void Download()
        {
            var dexter = new Media(@"SampleMedia\dexter.mp4");

            var subtitleBytes = subdb.DownloadSubtitleAsync(dexter.ComputeHash(), "en").Result;

            Assert.IsNotNull(subtitleBytes);
        }

        [TestMethod]
        public void SearchNotFound()
        {
            var dexter = new Media(@"SampleMedia\dexter.mp4");
            string wrongHash = dexter.ComputeHash().Substring(1) + "f";

            var results = subdb.SearchSubtitleAsync(wrongHash).Result;

            Assert.IsNull(results);
        }

        [TestMethod]
        public void DownloadNotFound()
        {
            var dexter = new Media(@"SampleMedia\dexter.mp4");
            string wrongHash = dexter.ComputeHash().Substring(1) + "f";

            var subtitleBytes = subdb.DownloadSubtitleAsync(wrongHash, "en").Result;

            Assert.IsNull(subtitleBytes);
        }
    }
}
