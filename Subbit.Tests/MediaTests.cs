﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Subbit.Core;

namespace Subbit.Tests
{
    [TestClass]
    public class MediaTests
    {
        [TestMethod]
        public void DexterComputeHash()
        {
            string expectedHash = "ffd8d4aa68033dc03d1c8ef373b9028c";
            var dexter = new Media(@"SampleMedia\dexter.mp4");

            string actualHash = dexter.ComputeHash();

            Assert.AreEqual(expectedHash, actualHash);
        }

        [TestMethod]
        public void JustifiedComputeHash()
        {
            string expectedHash = "edc1981d6459c6111fe36205b4aff6c2";
            var justified = new Media(@"SampleMedia\justified.mp4");

            string actualHash = justified.ComputeHash();

            Assert.AreEqual(expectedHash, actualHash);
        }

        [ExpectedException(typeof(FileNotFoundException))]
        [TestMethod]
        public void ComputeHashThrowsFileNotFound()
        {
            var nonExistentFile = new Media($"{Guid.NewGuid().ToString()}.mp4");
            nonExistentFile.ComputeHash();
        }
    }
}
