﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Subbit.CLI;

namespace Subbit.Tests
{
    [TestClass]
    public class RegistryManagerTests
    {
        [TestMethod]
        public void InstallAppPath()
        {
            RegistryManager.InstallAppPath("test123.exe", @"C:\NoWhere\test123.exe");
        }

        [TestMethod]
        public void UninstallAppPath()
        {
            RegistryManager.UninstallAppPath("test123.exe");
        }

        [TestMethod]
        public void InstallCommand()
        {
            RegistryManager.InstallCommand(".mp4", "notepad.exe");
        }

        [TestMethod]
        public void UninstallComamnd()
        {
            RegistryManager.UninstallCommand(".mp4");
        }
    }
}
